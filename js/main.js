class Vehicle {
    constructor(name, power, xPos, yPos) {
        this._element = document.createElement('img');
        this._name = name;
        this._power = power;
        this._xPos = xPos;
        this._yPos = yPos;
    }
    draw() {
        const container = document.getElementById('container');
        this._element.src = `./assets/images/${this._name}.jpg`;
        this._element.className = 'cars';
        this._element.id = this._name;
        container.appendChild(this._element);
        this.update();
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
    set xPos(distance) {
        this._xPos += distance;
        this._element.classList.add('drive');
        this.update();
    }
    setX(x) {
        this._xPos = x;
    }
    setY(y) {
        this._yPos = y;
    }
}
class Car extends Vehicle {
    constructor(name, power) {
        super(name, power, 800, 35);
    }
}
class Game {
    constructor() {
        this.keyDownHandler = (e) => {
            switch (e.keyCode) {
                case 37:
                    this._cars[0].xPos = -30;
                    this._element.classList.add('drive');
                    break;
                case 39:
                    this._cars[1].xPos = -50;
                    this._element.classList.add('drive');
                    break;
            }
        };
        this.transitionHandler = (e) => {
            this.collision();
        };
        this._element = document.getElementById('container');
        this._parking = new Parking("parking", 0, 0);
        this._turn = 0;
        this._players = new Array();
        this._cars = new Array();
        this._cars[0] = new Car("BMW", 90);
        this._cars[1] = new Car("FORD", 100);
        this._players[0] = new Player(0, "jantje", 0);
        this._players[1] = new Player(1, "pietje", 0);
        this.draw();
        window.addEventListener('keydown', this.keyDownHandler);
        window.addEventListener("transitionend", this.transitionHandler);
    }
    draw() {
        for (let i = 0; i < this._cars.length; i++) {
            this._cars[i].setX(800);
            this._cars[i].setY(35);
        }
        this._element.innerHTML = '';
        let sb = document.createElement("p");
        sb.className = "sb";
        sb.innerHTML += this._players[0].score;
        sb.innerHTML += "<br>";
        sb.innerHTML += this._players[1].score;
        this._element.appendChild(sb);
        this._parking.draw();
        let win = document.createElement("p");
        win.setAttribute("id", "win");
        this._element.appendChild(win);
        this.currentCar();
    }
    currentCar() {
        var randomCar = Math.floor(Math.random() * this._cars.length);
        this._cars.map((car, index) => {
            if (index == randomCar) {
                car.draw();
            }
        });
    }
    collision() {
        const carRect = document.getElementsByClassName('cars')[0].getBoundingClientRect();
        const Parking = document.getElementById('parking').getBoundingClientRect();
        if (carRect.left <= Parking.right && carRect.left >= Parking.left && carRect.right <= Parking.right) {
            if (this._turn == 0) {
                this._players[0].addscore(1);
                this._turn = 1;
            }
            else {
                this._players[1].addscore(1);
                this._turn = 0;
            }
            console.log("player " + (this._turn + 1));
            this.draw();
        }
        else if (carRect.left <= Parking.left) {
            if (this._turn == 0) {
                this._turn = 1;
                document.getElementById("win").style.opacity = "1";
                document.getElementById("win").innerHTML = "je bent er voorbij <br> De volgende is aan de beurt!" + this._players[1].name;
            }
            else {
                this._turn = 0;
                document.getElementById("win").style.opacity = "1";
                document.getElementById("win").innerHTML = "je bent er voorbij <br> De volgende is aan de beurt!" + this._players[0].name;
            }
            setTimeout(() => {
                this.draw();
            }, 3000);
        }
    }
}
let game;
var init = function () {
    game = new Game();
};
window.addEventListener("load", init);
class Parking {
    constructor(naam, xpos, ypos) {
        this._element = document.createElement('img');
        this._parking = naam;
        this._xPos = xpos;
        this._yPos = ypos;
    }
    draw() {
        const container = document.getElementById('container');
        this._element.id = "parking";
        container.appendChild(this._element);
    }
}
class Player {
    constructor(id, name, scoreboard) {
        this._id = id;
        this._name = name;
        this._scoreboard = scoreboard;
    }
    get name() {
        return this._name;
    }
    get score() {
        return this._name + ": " + this._scoreboard;
    }
    addscore(x) {
        this._scoreboard += x;
    }
}
//# sourceMappingURL=main.js.map