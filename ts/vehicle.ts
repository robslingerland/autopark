class Vehicle {

    protected _element: HTMLImageElement = document.createElement('img');
    protected _name: string;
    protected _power: number;
    protected _xPos: number;
    protected _yPos: number;

    constructor(name:string, power: number, xPos: number, yPos: number) {
        this._name = name;
        this._power = power;
        this._xPos = xPos;
        this._yPos = yPos;
    }

    public draw(){
        const container = document.getElementById('container');
        this._element.src = `./assets/images/${this._name}.jpg`;
        this._element.className = 'cars';
        this._element.id = this._name;
        container.appendChild(this._element);
        this.update();
    }

    public update(){
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        
    }

    public set xPos(distance: number) {
        this._xPos += distance; 
        this._element.classList.add('drive');
        this.update(); 
    } 

    public setX(x: number){
        this._xPos = x;
    }

    public setY(y: number){
        this._yPos = y;
    }
}